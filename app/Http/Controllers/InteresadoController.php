<?php

namespace App\Http\Controllers;

use App\Models\Interesado;
use App\Models\Proyecto;
use Illuminate\Http\Request;

use App\Http\Requests\StoreInteresadoRequest;

class InteresadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Interesado::with('proyecto')->paginate(30);
        return response()->json($proyectos);
    }
    public function export($tipo = '',$referencia_id = 0)
    {
        $headers = array(
            'Content-Type' => 'text/csv'
        );
        if (!File::exists(public_path()."/files")) {
            File::makeDirectory(public_path() . "/files");
        }
        $filename =  public_path("files/download.csv");
        $handle = fopen($filename, 'w');
        fputcsv($handle, [
            "ID",
            "Email",
        ]);
        $proyectos =[];
        if($tipo == ''){
            $proyectos = Proyecto::paginate(30);
            //return response()->json($proyectos);
        }else if($tipo == 'categoria'){
            $proyectos = Proyecto::where('categoria_id',$referencia_id)
                ->paginate(30);
            //return response()->json($proyectos);
        }
        foreach ($proyectos  as $proyecto) {
            fputcsv($handle, [
                $proyecto->name,
                $proyecto->email,
            ]);

        }
        fclose($handle);
        return  $filename;
        //return Response::download($filename, "download.csv", $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInteresadoRequest $request)
    {
        $mifecha= date('Y-m-d H:i:s'); 
        $fecha_inicio = date('Y-m-d H:i:s', strtotime ( '-24 hour' , strtotime ($mifecha) )) ; 
        $validar_interesado = Interesado::where('fecha', '>=', $fecha_inicio)
                                        ->where('fecha', '<=', $mifecha)
                                        ->where('correo', $request->correo)
                                        ->get();
        
        if($validar_interesado->count()==0){
            $interesado = new Interesado();
            $interesado->nombre = $request->nombre;
            $interesado->correo = $request->correo;
            $interesado->telefono = $request->telefono;
            $interesado->ciudad = $request->ciudad;
            $interesado->pais = $request->pais;
            $interesado->proyecto_id = $request->proyecto_id;
            $interesado->fecha = date("Y-m-d H:i:s");
            $result = $interesado->save();
            if($result){
                return response()->json([
                    'success'   => true,
                    'message'   => 'Registro Exitoso'
                ]);
            }else{
                return response()->json([
                    'success'   => false,
                    'message'   => 'Errores de Registro'
                ]);
            }
        }else{
            return response()->json([
                'success'   => false,
                'message'   => 'Errores de Validacion',
                'data'      => ["fecha" => "Ya existe una solicitud en trámite"]
            ]);
            /*
            dd([$validar_interesado->count(),$fecha_inicio,
            $mifecha]);
            */
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Interesado  $interesado
     * @return \Illuminate\Http\Response
     */
    public function show(Interesado $interesado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Interesado  $interesado
     * @return \Illuminate\Http\Response
     */
    public function edit(Interesado $interesado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Interesado  $interesado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interesado $interesado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Interesado  $interesado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interesado $interesado)
    {
        //
    }
}
