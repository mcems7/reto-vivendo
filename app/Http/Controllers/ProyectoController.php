<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use Illuminate\Http\Request;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tipo = '',$referencia_id = 0)
    {
        if($tipo == ''){
            $proyectos = Proyecto::paginate(30);
            return response()->json($proyectos);
        }else if($tipo == 'categoria'){
            $proyectos = Proyecto::where('categoria_id',$referencia_id)
                ->paginate(30);
            return response()->json($proyectos);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show($proyecto_id)
    {
        $proyecto = Proyecto::where('id', $proyecto_id)->first();
        return response()->json($proyecto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $proyecto)
    {
        echo "Update:".$proyecto;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy($proyecto)
    {
        echo "Delete:".$proyecto;
    }
}
