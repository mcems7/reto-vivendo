<?php

namespace App\Http\Controllers;

use App\Models\Constructora;
use Illuminate\Http\Request;

class ConstructoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Constructora  $constructora
     * @return \Illuminate\Http\Response
     */
    public function show(Constructora $constructora)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Constructora  $constructora
     * @return \Illuminate\Http\Response
     */
    public function edit(Constructora $constructora)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Constructora  $constructora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Constructora $constructora)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Constructora  $constructora
     * @return \Illuminate\Http\Response
     */
    public function destroy(Constructora $constructora)
    {
        //
    }
}
