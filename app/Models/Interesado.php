<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interesado extends Model
{
    use HasFactory;
    protected $table = 'interesado';
    protected $fillable = [
        'nombre',
        'correo',
        'telefono',
        'ciudad',
        'pais',
        'proyecto_id',
        'fecha',
    ];
    public function proyecto()
    {
        return $this->belongsTo('App\Models\Proyecto');
    }
}
