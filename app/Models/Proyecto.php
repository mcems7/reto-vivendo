<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;
    protected $table = 'proyecto';

    protected $fillable = [
        'nombre',
        'descripcion',
        'ciudad_id',
        'constructora_id',
        'categoria_id',
        'direccion',
        'num_habitaciones',
        'num_banos',
    ];

    public function interesados()
    {
        return $this->hasMany('App\Models\Interesado');
    }
}
