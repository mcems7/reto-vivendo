<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudad')->insert([
            'nombre' => 'Cali',
            'departamento_id' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('ciudad')->insert([
            'nombre' => 'Pasto',
            'departamento_id' => '2',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
