<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria')->insert([
            'nombre' => 'VIS',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categoria')->insert([
            'nombre' => 'SUPERIOR A VIS',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categoria')->insert([
            'nombre' => 'GOLD',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
