<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proyecto')->insert([
            'nombre' => 'Apartamentos el Lago',
            'descripcion' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt aspernatur fugiat quis nesciunt perspiciatis dolorem repellat corrupti cupiditate, unde fuga iure minima doloribus odio ipsum repudiandae cumque magni soluta consequatur.',
            'ciudad_id' => '1',
            'constructora_id' => '1',
            'categoria_id' => '1',
            'direccion' => 'Calle 1 No 2 - 3',
            'num_habitaciones' => '3',
            'num_banos' => '2',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('proyecto')->insert([
            'nombre' => 'Apartamentos el Prado',
            'descripcion' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt aspernatur fugiat quis nesciunt perspiciatis dolorem repellat corrupti cupiditate, unde fuga iure minima doloribus odio ipsum repudiandae cumque magni soluta consequatur.',
            'ciudad_id' => '2',
            'constructora_id' => '1',
            'categoria_id' => '1',
            'direccion' => 'Calle 2 No 3 - 4',
            'num_habitaciones' => '3',
            'num_banos' => '2',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
