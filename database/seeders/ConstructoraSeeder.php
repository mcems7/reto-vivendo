<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConstructoraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('constructora')->insert([
            'nombre' => 'Contruir',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
