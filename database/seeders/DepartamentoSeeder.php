<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departamento')->insert([
            'nombre' => 'Valle del Cauca',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Nariño',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
