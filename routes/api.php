<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\InteresadoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/proyectos', [ProyectoController::class, 'index']);
Route::post('/proyectos', [ProyectoController::class, 'store']);
Route::get('/proyectos/{categoria}/{referencia_id}', [ProyectoController::class, 'index']);
Route::get('/proyectos/{proyecto_id}', [ProyectoController::class, 'show']);
Route::post('/proyectos/{proyecto_id}', [ProyectoController::class, 'update']);
Route::delete('/proyectos/{proyecto_id}', [ProyectoController::class, 'destroy']);
Route::get('/interesados/export', [InteresadoController::class, 'export']);
Route::post('/interesados', [InteresadoController::class, 'store']);
Route::get('/interesados', [InteresadoController::class, 'index']);
