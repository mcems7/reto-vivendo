Instrucciones para desplegar

1. clonar el repositorio desde https://gitlab.com/mcems7/reto-vivendo
Comando:
git clone git@gitlab.com:mcems7/reto-vivendo.git

2. Instalar dependencias de composer
Comando:
composer update

3. Configurar Base de datos
Copiar el achivo .env.example y crear el archivo .env en la carpeta raiz

Modificar las variabes:
DB_CONNECTION=mysql -> Motor
DB_HOST=127.0.0.1 -> Servidor
DB_PORT=3306 -> Puerto
DB_DATABASE=vivendo ->Nombre de base de datos
DB_USERNAME=root -> Usuario e base de datos
DB_PASSWORD=**** -> Conraseña

4. Desplegar las migraciones
Opcional: usar el modificador --seed para ejecutar los seeder, la alternativa es ejecutar los Seeder de forma independiente a las migraciones
Comando:
php artisan migrate --seed

5. Iniciar servidor
Comando:
php artisan serve